/*
 
 File: LEPeripheralViewController.m
 
 Abstract: Interface to allow the user to enter data that will be
 transferred to a version of the app in Central Mode, when it is brought
 close enough.
 
 Version: 1.0
 
 Disclaimer: IMPORTANT:  This Apple software is supplied to you by
 Apple Inc. ("Apple") in consideration of your agreement to the
 following terms, and your use, installation, modification or
 redistribution of this Apple software constitutes acceptance of these
 terms.  If you do not agree with these terms, please do not use,
 install, modify or redistribute this Apple software.
 
 In consideration of your agreement to abide by the following terms, and
 subject to these terms, Apple grants you a personal, non-exclusive
 license, under Apple's copyrights in this original Apple software (the
 "Apple Software"), to use, reproduce, modify and redistribute the Apple
 Software, with or without modifications, in source and/or binary forms;
 provided that if you redistribute the Apple Software in its entirety and
 without modifications, you must retain this notice and the following
 text and disclaimers in all such redistributions of the Apple Software.
 Neither the name, trademarks, service marks or logos of Apple Inc.
 may be used to endorse or promote products derived from the Apple
 Software without specific prior written permission from Apple.  Except
 as expressly stated in this notice, no other rights or licenses, express
 or implied, are granted by Apple herein, including but not limited to
 any patent rights that may be infringed by your derivative works or by
 other works in which the Apple Software may be incorporated.
 
 The Apple Software is provided by Apple on an "AS IS" basis.  APPLE
 MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
 THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, REGARDING THE APPLE SOFTWARE OR ITS USE AND
 OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.
 
 IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL
 OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
 MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED
 AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE),
 STRICT LIABILITY OR OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Copyright (C) 2012 Apple Inc. All Rights Reserved.
 
 */

#import "BTLEPeripheralViewController.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import "TransferService.h"

static const NSUInteger     kProducerQueueLen       =  12; // maximum length of producer queue - new elements are not added if exceeding maximum
static const NSUInteger     kProducerRateMsgsPerSec = 100; // produce new messages at this rate
static const NSTimeInterval kProducerInterval       = 1.0f / kProducerRateMsgsPerSec;

@interface BTLEPeripheralViewController () <CBPeripheralManagerDelegate>
{
    uint32_t _serial;
    unsigned _overflowCounter;
    
    NSTimer        *_producerTimer;
    NSMutableArray *_producerQueue;
    NSDate         *_producerTimestamp; //  made to compensate for NSTimer not being perfect
}
@property (strong, nonatomic) IBOutlet UITextView       *textView;
@property (strong, nonatomic) IBOutlet UISwitch         *advertisingSwitch;
@property (strong, nonatomic) CBPeripheralManager       *peripheralManager;
@property (strong, nonatomic) CBMutableCharacteristic   *transferCharacteristic;
@end



#define NOTIFY_MTU      20



@implementation BTLEPeripheralViewController



#pragma mark - View Lifecycle



- (void)viewDidLoad
{
    [super viewDidLoad];

    // Start up the CBPeripheralManager
    _peripheralManager = [[CBPeripheralManager alloc] initWithDelegate:self queue:nil];

    _textView.userInteractionEnabled = NO; // not used in this test app
}


- (void)viewWillDisappear:(BOOL)animated
{
    // Don't keep it going while we're not showing.
    [self.peripheralManager stopAdvertising];

    [super viewWillDisappear:animated];
}



#pragma mark - Peripheral Methods



/** Required protocol method.  A full app should take care of all the possible states,
 *  but we're just waiting for  to know when the CBPeripheralManager is ready
 */
- (void)peripheralManagerDidUpdateState:(CBPeripheralManager *)peripheral
{
    // Opt out from any other state
    if (peripheral.state != CBPeripheralManagerStatePoweredOn) {
        return;
    }
    
    // We're in CBPeripheralManagerStatePoweredOn state...
    NSLog(@"self.peripheralManager powered on.");
    
    // ... so build our service.
    
    // Start with the CBMutableCharacteristic
    self.transferCharacteristic = [[CBMutableCharacteristic alloc] initWithType:[CBUUID UUIDWithString:TRANSFER_CHARACTERISTIC_UUID]
                                                                      properties:CBCharacteristicPropertyNotify
                                                                           value:nil
                                                                     permissions:CBAttributePermissionsReadable];

    // Then the service
    CBMutableService *transferService = [[CBMutableService alloc] initWithType:[CBUUID UUIDWithString:TRANSFER_SERVICE_UUID]
                                                                        primary:YES];
    
    // Add the characteristic to the service
    transferService.characteristics = @[self.transferCharacteristic];
    
    // And add it to the peripheral manager
    [self.peripheralManager addService:transferService];
}


/** Catch when someone subscribes to our characteristic, then start sending them data
 */
- (void)peripheralManager:(CBPeripheralManager *)peripheral central:(CBCentral *)central didSubscribeToCharacteristic:(CBCharacteristic *)characteristic
{
    NSLog(@"Central subscribed to characteristic");
    [_peripheralManager setDesiredConnectionLatency:CBPeripheralManagerConnectionLatencyLow forCentral:central];

    // Start sending
    [self producerStart];
    [self sendData];
}


/** Recognise when the central unsubscribes
 */
- (void)peripheralManager:(CBPeripheralManager *)peripheral central:(CBCentral *)central didUnsubscribeFromCharacteristic:(CBCharacteristic *)characteristic
{
    NSLog(@"Central unsubscribed from characteristic");
    [self producerStop];
}


/** Sends the next amount of data to the connected central
 */
- (void)sendData
{
    while ([_producerQueue count] > 0)
    {
        NSData *message = [_producerQueue objectAtIndex:0];
        BOOL didSend = [self.peripheralManager updateValue:message forCharacteristic:self.transferCharacteristic onSubscribedCentrals:nil];
        if (didSend)
            [_producerQueue removeObjectAtIndex:0];
        else
            break;
    }
}


/** This callback comes in when the PeripheralManager is ready to send the next chunk of data.
 *  This is to ensure that packets will arrive in the order they are sent
 */
- (void)peripheralManagerIsReadyToUpdateSubscribers:(CBPeripheralManager *)peripheral
{
    // Start sending again
    [self sendData];
}


#pragma mark - Switch Methods



/** Start advertising
 */
- (IBAction)switchChanged:(id)sender
{
    if (self.advertisingSwitch.on) {
        // All we advertise is our service's UUID
        [self.peripheralManager startAdvertising:@{ CBAdvertisementDataServiceUUIDsKey : @[[CBUUID UUIDWithString:TRANSFER_SERVICE_UUID]] }];
    }
    
    else {
        [self.peripheralManager stopAdvertising];
    }
}


-(void)producerStart
{
    NSLog(@"Starting producer");
    _serial          = 0;
    _overflowCounter = 0;
    _producerTimestamp = [NSDate date];
    
    _producerQueue = [[NSMutableArray alloc] initWithCapacity:kProducerQueueLen];
    _producerTimer = [NSTimer scheduledTimerWithTimeInterval:kProducerInterval target:self selector:@selector(producerTimeout:) userInfo:nil repeats:YES];
}

-(void)producerStop
{
    NSLog(@"Stopping producer");
    [_producerQueue removeAllObjects];
    [_producerTimer invalidate];
}

-(void)producerTimeout:(NSTimer *)timer
{
    NSDate *now = [NSDate date];

    while ([_producerTimestamp compare:now] == NSOrderedAscending) //  made to compensate for NSTimer not being perfect
    {
        _producerTimestamp = [_producerTimestamp dateByAddingTimeInterval:kProducerInterval];
        
        if ([_producerQueue count] < kProducerQueueLen)
        {
            message_t message;
            
            message.serial           = _serial;
            message.nbr_of_overflows = _overflowCounter;
            message.produce_rate     = kProducerRateMsgsPerSec;
            
            [_producerQueue addObject:[NSData dataWithBytes:&message length:sizeof(message)]];
        }
        else
            _overflowCounter++;
        
        if (_serial % kProducerRateMsgsPerSec == 0) // update every second to limit view rendering
            _textView.text = [[NSString alloc] initWithFormat:@"Producing %u\nProducer overflows: %u", _serial, _overflowCounter];
        
        _serial++;
    }
    [self sendData];
}

@end
